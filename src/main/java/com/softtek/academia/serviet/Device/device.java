package com.softtek.academia.serviet.Device;

public class device {

	int deviceid;
	String name;
	String description;
	int manufacturer;
	int color;
	String comments;
	
	public device(int deviceid, String name, String description, int manufacturer, int color, String comments) {
		super();
		this.deviceid = deviceid;
		this.name = name;
		this.description = description;
		this.manufacturer = manufacturer;
		this.color = color;
		this.comments = comments;
	}
	
	public int getDeviceid() {
		return deviceid;
	}

	public void setDeviceid(int deviceid) {
		this.deviceid = deviceid;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public int getManufacturer() {
		return manufacturer;
	}

	public void setManufacturer(int manufacturer) {
		this.manufacturer = manufacturer;
	}

	public int getColor() {
		return color;
	}

	public void setColor(int color) {
		this.color = color;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}
	@Override
	public String toString(){
		return deviceid+ " " + name + " " +  description + " " + manufacturer +" "+  color + " " +  comments;
	}


	
}
