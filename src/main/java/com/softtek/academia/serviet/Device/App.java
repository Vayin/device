package com.softtek.academia.serviet.Device;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.OptionalLong;
import java.util.stream.Collectors;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
    	System.out.println("MySQL JDBC Connection");

        List<device> result = new ArrayList<>();

        String SQL_SELECT = "Select * from DEVICE";
        
    	// auto close connection
        try  {
                Connection conn = dbConnector.getConnector();
                PreparedStatement preparedStatement = conn.prepareStatement(SQL_SELECT);

                ResultSet resultSet = preparedStatement.executeQuery();
                
                List<device> list = new ArrayList<>(); 

                while (resultSet.next()) {
                    int deviceid = resultSet.getInt("DEVICEID");
                    String name = resultSet.getString("NAME");
                    String description = resultSet.getString("DESCRIPTION");
                    int manufacturerid = resultSet.getInt("MANUFACTURERID");
                    int colorid = resultSet.getInt("COLORID");
                    String comments = resultSet.getString("COMMENTS");
                    
                    System.out.println("DEVICEID: " + deviceid + " NAME: " + name + " DESCRIPTION: " + description + " MANUFACTURERID: " + manufacturerid + " COLORID: " + colorid + " COMMETS: " + comments);
                    
            device lista = new device(deviceid,name,description, manufacturerid, colorid, comments);
            		list.add(lista);
            		
            		
                }
                
                List<String> mapeo = list.stream()
                		.map((device1)->device1.getName())
                		.collect(Collectors.toList());
                		System.out.println(mapeo);
                		
                		
               	List<String> filtro = list.stream()
                  		.filter((device1)->device1.getName().equals("Laptop"))
                  		.map((device1)->device1.getName())
                  		.collect(Collectors.toList());
                         System.out.println(filtro);
                 
                         
                         
                 List<device> filtro2 = list.stream()
                           .filter((device2)->device2.getColor() == 1)
                           .collect(Collectors.toList());
                 for (device device : filtro2) {
                	 System.out.println(device.getName());        
             				
				}
                          
                 
                 Long filtro3 = list.stream()
                         .filter((device2)->device2.getManufacturer() == 3)
                         .count() ;                      		 ;
              
              	 System.out.println(filtro3);        
                 
                 Map<Integer,device> mapeo1 = list.stream().collect(Collectors.toMap(device::getDeviceid, 
                		 (d)-> new device (d.getDeviceid(),d.getName(),d.getDescription(),d.getManufacturer(),d.getColor(),d.getComments())));
                 System.out.println(mapeo1.values().toString());
           				
				
            
            
            
          
            
            

        } catch (SQLException e) {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } catch (Exception e) {
            e.printStackTrace();
        }  
    }
}
